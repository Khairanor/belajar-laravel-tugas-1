<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regis()
    {
        return view('page.register');
    }

    public function welcome(Request $request)
    {
        //dd($request->all());
        $nama = $request["nama"];
        $nama2 = $request["nama2"];
        return view('page.welcome', compact("nama","nama2"));
    }
}
